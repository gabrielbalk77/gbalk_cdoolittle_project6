package LibraryCatalogSystem;

public interface iLibrary {
	//This method will iterate over all of the lines in the input file and create the right type of
	//library item and then add them to an array of libraryItems
	void createLibraryCollection();
	
	//This is a helper method for the createLibraryCollection()
	//It creates a new book
	Book createNewBook(String[] temp);
	
	//This is a helper method for the createLibraryCollection()
	//It creates a periodical
	Periodical createNewPeriodical(String[] temp);
	
	//This should create a string of all of the information about the items in the library so they can be passed to the GUI and printed out
	String printLibraryItems();
	
	//This is a support method for checkOutProcess()
	String checkOut(LibraryItem item);
		
	//This Method will do the needed steps to check out a book and it also returns a message containing what was doesn't and what happened. 
	//Takes in the callNumber as a string from the user
	String checkOutProcess(String userInput);
}