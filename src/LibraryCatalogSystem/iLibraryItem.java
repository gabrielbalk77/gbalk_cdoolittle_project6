package LibraryCatalogSystem;

import java.util.GregorianCalendar;

public interface iLibraryItem {

	//returns the call number
	public String getCallNumber();
	
	//returns the item title
	public String getTitle();
	
	//returns a boolean based on if a library item is checked out
	public boolean getIsCheckedOut();
	
	//sets a library item as checked out
	public void setIsCheckedOut(boolean isCheckedOut);
	
	//prints information about a library item
	abstract public String printOtherInfo(LibraryItem item);
	
	//sets the date that a library item will be due
	public abstract void setDueDate();
	
	//returns the date a library item was checked out
	public GregorianCalendar getCheckedOutDate();
	
	//returns the date a library item is due
	public GregorianCalendar getDueDate();
}
