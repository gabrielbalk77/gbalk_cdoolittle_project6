package LibraryCatalogSystem;

//Import necessary packages
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Periodical class that is a sublass of the LibraryItem superclass.
 * @author Gabriel Balk and Casey Doolittle
 *
 */
public class Periodical extends LibraryItem{

	//Instantiate fields with the self explanatory names
	private String volume;
	private String issue;
	private String subject;

	
	/**
	 * Constructor for all specific field and passes general feilds to super class
	 * @param callNumber
	 * @param title
	 * @param volume
	 * @param issue
	 * @param subject
	 */
	public Periodical(String callNumber, String title, String volume, String issue, String subject) {
		super(callNumber, title);
		this.volume = volume;
		this.issue = issue;
		this.subject = subject;
	}

	/**
	 * This sets the due date specific for the periodical.
	 */
	public void setDueDate() {
		checkOutDate = new GregorianCalendar();
		dueDate = (GregorianCalendar)checkOutDate.clone();
		dueDate.add(Calendar.DAY_OF_YEAR, 7);
	}

	
	/**
	 * This returns all of the item/Periodical specific information
	 * @return specific String information about a library item
	 */
	public String printOtherInfo(LibraryItem item) {
		String specific = ", Volume: " + volume + ", Issue: " + issue + ", Subject: " + subject;

		if(item.getIsCheckedOut() == true) {
			specific += String.format(", Date Out: %tD\n", getCheckedOutDate()) +  String.format(", Due Date: %tD\n", getDueDate());
		}

		return specific;
	}
	
	
	/*
	 * Getters and Setters
	 */
	
	/**
	 * Returns the volume of the Periodical
	 * @return volume
	 */
	public String getVolume() {
		return volume;
	}

	/**
	 * Returns the issue of the Periodical
	 * @return issue
	 */
	public String getIssue() {
		return issue;
	}

	/**
	 * Returns the subject of the Periodical
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}
}
