package LibraryCatalogSystem;

//Import necessary packages
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Book class that is a sublass of the LibraryItem superclass.
 * @author Gabriel Balk and Casey Doolittle
 *
 */
public class Book extends LibraryItem{

	//Instantiate fields with the self explanatory names
	private String author;
	private String genre;

	
	/**
	 * Constructor for all specific field and passes general feilds to super class
	 * @param callNumber
	 * @param title
	 * @param author
	 * @param genre
	 */
	public Book(String callNumber, String title, String author, String genre) {
		super(callNumber, title);
		this.author = author;
		this.genre = genre;
	}

	
	/**
	 * This sets the due date specific for the book
	 */
	public void setDueDate() {
		checkOutDate = new GregorianCalendar();
		dueDate = (GregorianCalendar)checkOutDate.clone();
		dueDate.add(Calendar.DAY_OF_YEAR, 21);
	}

	
	/**
	 * This returns all of the item/Periodical specific information
	 * @param item
	 * @return String containing formatted checkout date and due date.
	 */
	public String printOtherInfo(LibraryItem item) { 

		String specific = ", Author: " + author + ", Genre: " + genre;

		if(item.getIsCheckedOut() == true) {
			specific += String.format(", Date Out: %tD\n", getCheckedOutDate()) +  String.format(", Due Date: %tD\n", getDueDate());
		}
		return specific;
	}
	
	/*
	 * Getters and Setters
	 */
	
	/**
	 * Returns the author of the Book
	 * @return author
	 */
	String getAuthor() {
		return author;
	}

	/**
	 * Returns the genre of the Book
	 * @return genre
	 */
	public String getGenre() {
		return genre;
	}
}
