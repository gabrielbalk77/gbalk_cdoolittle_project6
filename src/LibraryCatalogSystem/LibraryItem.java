package LibraryCatalogSystem;

//Import necessary packages
import java.util.GregorianCalendar;

/**
 * LibraryItem class that acts as a superclass for the Book and Periodical classes.
 * @author Gabriel Balk and Casey Doolittle
 *
 */
public abstract class LibraryItem implements iLibraryItem{
	
	//Instantiate fields with the self explanatory names
	protected String callNumber;
	protected String title;
	protected boolean checkedOut;
	protected GregorianCalendar checkOutDate;
	protected GregorianCalendar dueDate;
	

	/**
	 * Constructor for all of the fields
	 * @param callNumber
	 * @param title
	 */
	public LibraryItem(String callNumber, String title) {
		this.callNumber = callNumber;
		this.title = title;
		checkedOut = false;
	}
	
	/*
	 * BELOW ARE GETTERS AND SETTERS
	 * Also some abstract methods defined in subclasses
	 */

	/**
	 * Returns the call number of the Book
	 * @return callNumber
	 */
	public String getCallNumber() {
		return callNumber;
	}
	/**
	 * Returns the item title
	 * @return title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Returns a boolean based on if a library item is checked out
	 * @return checkedOut
	 */
	public boolean getIsCheckedOut() {
		return checkedOut;
	}
	
	/**
	 * Sets a library item as checked out
	 */
	public void setIsCheckedOut(boolean isCheckedOut) {
		checkedOut = isCheckedOut;
	}
	
	/**
	 * Prints information about a library item
	 * @return String containing information about the library item
	 */
	abstract public String printOtherInfo(LibraryItem item);
	
	/**
	 * Sets the date that a library item will be due
	 */
	abstract public void setDueDate();
	
	/**
	 * Returns the date a library item was checked out
	 * @return checkoutDate
	 */
	public GregorianCalendar getCheckedOutDate() {
		return checkOutDate;
	}
	
	/**
	 * Returns the date a library item is due
	 * @return dueDate
	 */
	public GregorianCalendar getDueDate() {
		return dueDate;
	}
}
