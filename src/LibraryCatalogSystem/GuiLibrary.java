package LibraryCatalogSystem;

//Import necessary packages
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * GuiLibrary class that establishes a graphic user interface and takes
 * input from the user.
 * @author Gabriel Balk and Casey Doolittle
 *
 */
public class GuiLibrary extends JFrame{

	
	//Instantiate fields with the self explanatory names
	private JPanel panel;
	private JButton checkOutButton;
	private JButton printCatalogButton;
	private JTextField textField;
	private JLabel label1;
	private JLabel label2;
	private final int WINDOW_WIDTH = 900, WINDOW_HEIGHT = 150;
	public Library library;
	
	
	/**
	 * Constructor for the Gui, set up what to do when the close button is hit, create a new library
	 */
	public GuiLibrary() {
		library = new Library();
		library.createLibraryCollection();
		setTitle("Library Catalog");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		buildPanel();
		add(panel);
		setVisible(true);
	}

	/**
	 * Set up the panel, what it does, and what it looks like and says
	 */
	private void buildPanel() {
		label1 = new JLabel("Welcome to Van Wylen Library. To check a book out enter the Call Number and to see whats in the library press button to the right.");
		label2 = new JLabel("Enter Library Item");
		textField = new JTextField(10);
		checkOutButton = new JButton("Check Out");
		checkOutButton.addActionListener(new CheckOutButtonListener());
		printCatalogButton = new JButton("Print Catalog");
		printCatalogButton.addActionListener(new PrintCatalogButtonListener());
		panel = new JPanel();
		panel.add(label1);
		panel.add(printCatalogButton);
		panel.add(label2);
		panel.add(textField);
		panel.add(checkOutButton);
	}
	
	
	/**
	 * This method listens for if the check out button is hit and if it is it will execute the needed code 
	 * @author Gabriel Balk and Casey Doolittle
	 */
	private class CheckOutButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String userInput = textField.getText();
			if(userInput == "") {
				JOptionPane.showMessageDialog(panel, "Please Enter Something.");
			}
			else {
				JOptionPane.showMessageDialog(panel, library.checkOutProcess(userInput));
			}
		}
	}
	
	/**
	 * This listens for when the print catalog button is hit and when it is calls needed code to show the catalog
	 * @author Gabriel Balk and Casey Doolittle
	 */
	private class PrintCatalogButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String libraryItems = library.printLibraryItems();
			JOptionPane.showMessageDialog(panel, libraryItems);
		}
	}
}
