package LibraryCatalogSystem;

/**
 * @author: Gabriel Balk and Casey Doolittle
 * 
 * This the the Project 6 Main where the parts that are needed for running
 * the program are called and run.
 * 
 * The purpose of the LibraryCatalogSystem is to be able to view the library
 * books and periodicals in the proper way for each type of item and be able 
 * to check them out and also to keep track of due dates and other information.
 */

public class Project6Main {
	
	/**
	 * This creates a new gui where the rest of the methods and procedures get called with input from the user
	 * @param args
	 */
	public static void main(String[] args) {
		GuiLibrary gui = new GuiLibrary();
	}
}