package LibraryCatalogSystem;

//Import necessary packages
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Library class that establishes a library which contains a collection of library items.
 * @author Gabriel Balk and Casey Doolittle
 *
 */
public class Library implements iLibrary{
	
	//Instantiate fields with the self explanatory names
	public LibraryItem[] libraryCollection;
	private static final int inputFileSize = 4;
	private Scanner scan;

	
	
	/**
	 * A constructor that will create the library collection
	 */
	public Library() {
		createLibraryCollection();
	}

	
	/**
	 * This method will iterate over all of the lines in the input 
	 * file and create the right type of library item and then add them 
	 * to an array of libraryItems
	 */
	public void createLibraryCollection() {
		
		libraryCollection = new LibraryItem[inputFileSize];
		
		try {
			File file = new File("LibraryCatalog.txt");
			scan = new Scanner(file);
		}
		catch(FileNotFoundException e) {
			System.out.println("This should never print becuase it is hardcoded in correctly");
		}
		
		for(int i = 0; i < inputFileSize; i++) {
			String[] temp = scan.nextLine().split(",");
			if(temp[0].equals("P")) {
				libraryCollection[i] = createNewPeriodical(temp);
			}
			
			else if(temp[0].equals("B")) {
				libraryCollection[i] = createNewBook(temp);
			}
		}
	}

	
	
	/**
	 * This is a helper method for the createLibraryCollection() which creates a new book
	 * @param temp
	 * @return book The Book object that was created.
	 */
	public Book createNewBook(String[] temp) {
		Book book = new Book(temp[1], temp[2], temp[3], temp[4]);
		return book;
	}
	
	
	/**
	 * This is a helper method for the createLibraryCollection() which creates a periodical
	 * @param temp
	 * @return periodical The Periodical object that was created.
	 */
	public Periodical createNewPeriodical(String[] temp) {
		Periodical periodical = new Periodical(temp[1], temp[2], temp[3], temp[4], temp[5]);
		return periodical;
	}

	/**
	 * This creates a string of all of the information 
	 * about the items in the library so they can be passed to the GUI and printed out.
	 * @return libraryInfo String containing information about the library item.
	 */
	public String printLibraryItems() {
		String libraryInfo = "";
		//System.out.println(libraryCollection[0]);
		for(LibraryItem item : libraryCollection) {
			libraryInfo += "Title: " + item.getTitle()+", Call Number: "+item.getCallNumber()+", Checked Out: "+item.getIsCheckedOut()+ item.printOtherInfo(item)+"\n";
		}	
		return libraryInfo;
	}

	/**
	 * This is a support method for checkOutProcess()
	 * @param item
	 * @return String telling the user if the book was checked out successfully or unsuccessfully.
	 */
	public String checkOut(LibraryItem item) {
		if(!item.getIsCheckedOut()) {
			item.setIsCheckedOut(true);
			item.setDueDate();
			return "Succesfully Checked Out";
		}
		else {
			return "This item is already checked out!";
		}
	}

	/**
	 * This Method will do the needed steps to check out a book and it also
	 * returns a message containing what happened. 
	 * @param userInput
	 * @return message String that will tell the user if the book is within the library.
	 */
	public String checkOutProcess(String userInput) {
		String message = "This book is not in the library";
		for(LibraryItem item : libraryCollection) {
			if(item.getCallNumber().equals(userInput)) {
				message = checkOut(item);
			}
		}
		return message;
	}
}
