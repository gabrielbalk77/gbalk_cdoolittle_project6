package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.print.Book;

import org.junit.jupiter.api.Test;

import LibraryCatalogSystem.Library;
import LibraryCatalogSystem.LibraryItem;

/**
 * Test class that will ensure methods are working correctly.
 * @author Gabriel Balk and Casey Doolittle
 * 
 */
class LibraryTest {

	@Test
	/**
	 * Test method that will compare expected results to items returned from book methods.
	 */
	void testBook() {
		Library lib = new Library();
		lib.createLibraryCollection();
		String[] bookList = {"B","C124.S17","The Cat in the Hat","Dr. Seuss","Children�s Literature"};
		LibraryItem book = lib.createNewBook(bookList);
		assertEquals("Succesfully Checked Out", lib.checkOut(book));
		assertEquals("This item is already checked out!", lib.checkOut(book));
	}
	@Test
	/**
	 * Test method that will compare expected results to items returned from periodical methods.
	 */
	void testPeriodical() {
		Library lib = new Library();
		String[] periodicalList = {"P","QJ072.C23.37.4","Computational Linguistics","37","4","Computational Linguistics"};
		LibraryItem periodical = lib.createNewPeriodical(periodicalList);
		assertEquals("Succesfully Checked Out", lib.checkOut(periodical));
		assertEquals("This item is already checked out!", lib.checkOut(periodical));
	}
}
